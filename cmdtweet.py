#!/usr/bin/env python
# Copyright © 2014 Carl Chenet <carl.chenet@mytux.fr>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import sys
import os.path
from twitter import *

class SendNewTweet:
    def __init__(self):
        CONSUMER_KEY = "xxxxxxxxxxxxxxxxxxxxxxxxx"
        CONSUMER_SECRET = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

        MY_TWITTER_CREDS = os.path.expanduser('credentials')
        if not os.path.exists(MY_TWITTER_CREDS):
            oauth_dance("cmdtweet", CONSUMER_KEY, CONSUMER_SECRET,
                        MY_TWITTER_CREDS)

        oauth_token, oauth_secret = read_token_file(MY_TWITTER_CREDS)

        foptwitter = Twitter(auth=OAuth(
            oauth_token, oauth_secret, CONSUMER_KEY, CONSUMER_SECRET))

        # Now work with Twitter
        foptwitter.statuses.update(status=sys.argv[1])

if __name__ == "__main__":
    SendNewTweet()
